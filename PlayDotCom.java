public class PlayDotCom {
  public static void main(String[] args) {
      DotCom game = new DotCom();
      CliReader reader = new CliReader();

      while (!game.over()) 
      {
          System.out.println(
                  game.makeMove(reader.input("Enter a number: "))
                  );
      }
      System.out.println(game.displayResults());
  }
}
