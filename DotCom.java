public class DotCom {
  private int[] locations;
  private int hits = 0;
  private int guesses = 0;

  public DotCom() {
      int start = (int)(Math.random() * 5);
      this.locations = new int[] {start, start + 1, start + 2};
  }

  public void setLocationCells(int[] setLocations) {
      locations = setLocations;
  }

  public boolean over()
  {
      return this.hits > 2;
  }

  public String makeMove(String stringGuess) {
      int guess = Integer.parseInt(stringGuess);

      this.guesses++;

      for (int location : locations) {
          if (location == guess) {
              this.hits++;
              if (this.hits > 2)
                  return "kill";
              int[] nextArray = new int[locations.length-1];
              //System.out.println(nextArray.length);
              //for (int x : nextArray)
                  //System.out.println(x);
              int y = 0;
              for (int b : locations){
                  if (b != guess){
                      nextArray[y] = b;
                      System.out.println(b);
                      y++;
                  }
                  setLocationCells(nextArray);
              }
              return "hit";
          }
      }
      return "miss";
  }
  public String displayResults() {
      return "You took " + this.guesses + " guesses.";
  }
}
